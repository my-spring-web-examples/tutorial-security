CREATE TABLE account
(
    username   varchar(128),
    password   varchar(88),
    first_name varchar(128),
    last_name  varchar(128),
    constraint pk_tbl_account primary key (username)
);

INSERT INTO account(username, password, first_name, last_name)
VALUES ('demo', '2ac1e999b78b517036434026181d714ee1ab79a8b1e02ce15a6e67465d3b7798eaa7d9b72d43cc4b', 'Taro',
        'Yamada');
