package com.example.tutorialsecurity.app.welcome;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/welcome")
public class WelcomeController {
    @GetMapping("/home")
    public String view() {
        return "welcome/home";
    }
}
