package com.example.tutorialsecurity.domain.service.account;


import com.example.tutorialsecurity.common.exception.ResourceNotFoundException;
import com.example.tutorialsecurity.domain.model.Account;
import com.example.tutorialsecurity.domain.repository.account.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AccountSharedServiceImpl implements AccountSharedService {
    private final AccountRepository accountRepository;

    @Transactional(readOnly = true)
    @Override
    public Account findOne(String username) {
        return accountRepository.findById(username).orElseThrow(() -> new ResourceNotFoundException(
            "The given account is not found! username=" + username));
    }
}
