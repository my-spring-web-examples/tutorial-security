package com.example.tutorialsecurity.domain.service.account;

import com.example.tutorialsecurity.domain.model.Account;

public interface AccountSharedService {
    Account findOne(String username);
}
