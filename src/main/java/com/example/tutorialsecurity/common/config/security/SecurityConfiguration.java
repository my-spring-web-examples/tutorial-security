package com.example.tutorialsecurity.common.config.security;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
public class SecurityConfiguration {
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.formLogin(login -> login
                // この指定必須だった。デフォルト設定のはずだが、個々に書かないとpermitAllされないかもしれない
                .loginProcessingUrl("/login")
                .loginPage("/login/loginForm")
                .defaultSuccessUrl("/login/loginForm?error=false")
                .failureUrl("/login/login-error?error=true")
                .permitAll())
            .authorizeHttpRequests(
                authorize -> authorize
                    // CSS 読み込みに必要
                    .requestMatchers(PathRequest.toStaticResources().atCommonLocations())
                    .permitAll()
                    // 下記ではCSS読み込めず
//                    .mvcMatchers("/resources/**").permitAll()
                    .anyRequest().authenticated()

            ).logout(logout -> logout
                .logoutSuccessUrl("/")
                .deleteCookies("JSESSIONID")
            );
        return http.build();
    }

    // 下記の設定は不要
//    void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(sampleUserDetailService).passwordEncoder(passwordEncoder());
//    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new Pbkdf2PasswordEncoder();
    }
}
