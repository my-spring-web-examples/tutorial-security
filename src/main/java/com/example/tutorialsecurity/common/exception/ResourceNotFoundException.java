package com.example.tutorialsecurity.common.exception;

import lombok.experimental.StandardException;

@StandardException
public class ResourceNotFoundException extends RuntimeException{
}

